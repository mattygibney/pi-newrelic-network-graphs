# pi-newrelic-network-graphs

A quick and dirty way to run periodic network test (download/upload), get the CPU temp of the PI and send the data to newrelic. Although not the cleanest way to achive this it works and is fairly light. 

Hardware:
1 raspberry pi

Software:
Curl - Post json payload to newrelic 
Crontab - run commands every min/5mins
jq - parse and create the json payloads 
speedtest-cli - run a speedtest and produce a json output. 

In these commands we are using jq to modify the JSON into a custom event payload and post this to newrelic custom event api. You will need to get an API key from newrelic .

![image of newrelic dashboard](pi-dashboard.png)

The json export of the newrelic dashboard is almost ready to go, your account number just needs to be subbed in where $$ACCOUNTNO$$ is.
